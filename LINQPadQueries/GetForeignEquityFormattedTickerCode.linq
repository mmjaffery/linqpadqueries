<Query Kind="Program" />

static void Main()
{
	  string one = "MVN";
	  string two = "MVN CN";
	  string three = "MVN CN CV";
	  string jibberish = "SOME BIG STRING CASE THAT WE DONT ACCOUNT FOR"; 
	  
	  (one + "  is now " + GetForeignEquityFormattedTickerCode(one)).Dump();
	  (two + " is now " + GetForeignEquityFormattedTickerCode(two)).Dump();
	  (three + " is now " + GetForeignEquityFormattedTickerCode(three)).Dump();
	  (jibberish + " is now " + GetForeignEquityFormattedTickerCode(jibberish)).Dump();
	  
}

// Define other methods and classes here
public static string GetForeignEquityFormattedTickerCode(string targetCode)
{
	            var tickerArray = targetCode.Split(' ');
	            string finalTicker = "";
	            string space = " ";
	
	            switch (tickerArray.Length)
	            {
	                case 1:
	                    finalTicker = tickerArray[0];
	                    break;
	                case 2:
	                    finalTicker = tickerArray[0] + space + tickerArray[1];
	                    break;
	                case 3:
	                    finalTicker = tickerArray[0] + space + tickerArray[2];
	                    break;
	                default:
	                    finalTicker = string.Join(space, tickerArray);
	                    break;
	            }
	
	            return finalTicker;
	
}