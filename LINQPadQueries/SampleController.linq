<Query Kind="Program">
  <Namespace>System</Namespace>
  <Namespace>System.Collections.Generic</Namespace>
  <Namespace>System.Linq</Namespace>
  <Namespace>System.Web</Namespace>
  <Namespace>System.Web.Mvc</Namespace>
  <Namespace>WFA.Data.Interfaces</Namespace>
  <Namespace>WFA.Data.Repository</Namespace>
</Query>

void Main()
{
}


    //shouldn't be accessing the DBContext Class in the controller. 
    //Use repo instance instead. 
    //using constructor dependency injection pattern
    //Notice the code interacts with an abstract interface(IDataRepository)
    //and not the concrete implementation (DataRepository) of the interface
    [Obsolete]
    public class PortalUsersController : Controller
    {
        //
        // GET: /PortalUsers/
        private IDataRepository _repository;

        //ctor parameterless
        //called when application is running
        //creates instance of repository class and passes it to second constructor
        public PortalUsersController():this(new SQLRepository())
        {
            
        }

        //ctor that takes one argument/parameter
        //which is a repo instance that gets 
        //assigned to class level field name _repository
        public PortalUsersController(IDataRepository repository)
        {
            _repository = repository;

        }

        public ActionResult Index()
        {
            return View();
            //example 
            //return View(_repository.ListAll());
        }


        //TODO: //For testing 
        //For testing this class you can pass fake repository class to the HomeController 
        //You can implement IDataRepository class with a class that does not access the
        //database but contains all of the required methods of the IDataRepository interface 
        //that way you can unit Test this class without a real database in place like a object see repository class. 
    }
