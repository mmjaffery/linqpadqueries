<Query Kind="Program" />

void Main()
{
	List<Payment> paymentList = CreatePaymentObj();
	paymentList.Dump();
	var groupedSummedObj = from pymt in paymentList
						   group pymt by pymt.Invoice into pymtGroup
						   select new 
						   {				
						   		Customer = pymtGroup.Select(y => y.Customer).Distinct(),
								Bank = "01",
								PaymentReference = pymtGroup.Select(g => g.PaymentReference),
								TransactionType = pymtGroup.Key,
						   		Invoice = pymtGroup.Key , 
								PaymentValue = pymtGroup.Sum(x => x.GrossPaymentValue)
						   };
	groupedSummedObj.Dump();

}

List<Payment> CreatePaymentObj()
{
	List<Payment> paymentList = new List<UserQuery.Payment>()
	 {
	 	new Payment{ Customer = 5058811, Bank= "01", PaymentReference = "Blah1", TransactionType = "C", Invoice=21656049, GrossPaymentValue=-291.8 },
		new Payment{ Customer = 5058811, Bank= "01", PaymentReference = "Blah2", TransactionType = "I", Invoice=22563463, GrossPaymentValue=164.52 },
		new Payment{ Customer = 5058811, Bank= "01", PaymentReference = "Blah2", TransactionType = "I", Invoice=22563463, GrossPaymentValue=291.8 },
		new Payment{ Customer = 5058811, Bank= "01", PaymentReference = "Blah2", TransactionType = "I", Invoice=22563463, GrossPaymentValue=154.73 },
		new Payment{ Customer = 5058811, Bank= "01", PaymentReference = "Blah5", TransactionType = "C", Invoice=22644679, GrossPaymentValue=-154.73 }
	 };
	return paymentList;
}

public class Payment
{
	public int Customer { get; set; }
	public string Bank { get; set; }
	public string PaymentReference { get; set; }
	public string TransactionType { get; set; }
	public int Invoice { get; set; }
	public Double GrossPaymentValue { get; set; }
}
// Define other methods and classes here