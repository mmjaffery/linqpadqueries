<Query Kind="Program" />

void Main()
{
	//var withDupes = CreateTickerList().AsEnumerable().Distinct();//77 items
	var withDupes = CreateTickerList().AsEnumerable().Distinct(new DistinctItemComparer());//72 items
	HashSet<TickerCategory> noDupes = new HashSet<TickerCategory>(withDupes);	
	noDupes.Dump();
}

 public class TickerCategory
    {
        public string TKR { get; set; }
        public string SECCAT { get; set; }
    }

public class DistinctItemComparer : IEqualityComparer<TickerCategory>
    {
        public bool Equals(TickerCategory x, TickerCategory y)
        {
            return x.TKR == y.TKR &&
                   x.SECCAT == y.SECCAT; 
        }

        public int GetHashCode(TickerCategory obj)
        {
            return obj.TKR.GetHashCode();
        }
    }
		
		
public List<TickerCategory> CreateTickerList()
        {
            return new List<TickerCategory>()
            {
                new TickerCategory{ TKR = "5970 JP", SECCAT = "EQUITY"}, 
                new TickerCategory{ TKR = "RHP", SECCAT = "EQUITY"},
				new TickerCategory{ TKR = "5970 JP", SECCAT = "EQUITY"}, 
				new TickerCategory{ TKR = "5970 JP", SECCAT = "EQUITY"}, 
				new TickerCategory{ TKR = "5970 JP", SECCAT = "EQUITY"}, 
				new TickerCategory{ TKR = "5970 JP", SECCAT = "EQUITY"}, 
				new TickerCategory{ TKR = "5970 JP", SECCAT = "EQUITY"}, 
                new TickerCategory{ TKR = "AKER NO", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "8304 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "APAM NA", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "9948 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "73 HK", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "AUSS NO", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "6845 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "BALN EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "8332 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "BEZ LN", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "BEFB EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "BVS LN", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "BWO AV", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "BZU EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "CGL LN", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "6366 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "4617 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "COLT LN", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "CFEB EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "1828 HK", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "9793 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "DNORD DC", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "DCG AU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "DL EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "4061 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "GIL EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "006040 KS", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "296 HK", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "163 HK", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "GAM SW", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "GDC CN", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "GGR SP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "GNC AU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "GRNG SS", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "GRG EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "SIMECB M", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "3593 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "6856 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "4062 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "IIA EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "INDUC SS", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "INVP LN", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "1662 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "KWE LN", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "DSM NA", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "5463 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "3088 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "MRVE3 BZ", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "BWNG LN", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "5214 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "8327 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "ORA AU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "PARG SW", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "PGS EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "RHM EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "8140 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "SAFT EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "029780 KS", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "006400 KS", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "6844 jp", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "6379 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "9412 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "SKG LN", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "7718 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "TLX1 EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "5949 JP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "VK EU", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "VARD SP", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "MRW LN", SECCAT = "FRNEQUTY"},
                new TickerCategory{ TKR = "551 HK", SECCAT = "FRNEQUTY"}
            };
        }