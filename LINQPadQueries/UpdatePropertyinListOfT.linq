<Query Kind="Program" />

void Main()
{
	
	List<MortgageFields> VariableList = new List<MortgageFields> {
	new MortgageFields { LastRefixDate = "", CalculatedOutputDate = "01/01/2015"},
	new MortgageFields { LastRefixDate = "", CalculatedOutputDate = "01/02/2015"},
	new MortgageFields { LastRefixDate = "", CalculatedOutputDate = "01/04/2015"}
	};
	
	
	var someObject = VariableList.Where(x => x.LastRefixDate == "" )
	                                         .Select(x =>
	                                         {
	                                             x.LastRefixDate = x.CalculatedOutputDate;
	                                             return x;
	                                         });
	
	someObject.ToList().Dump();
}

// Define other methods and classes here
   class MortgageFields
    {
        public string TickerCode { get; set; }
        public string YellowKey { get; set; }
        //Factor Fields
        public string MortgageFactor { get; set; }
        public int? PayDelayDays { get; set; }
        public string PayDelayDescription { get; set; }
        public string PayDate { get; set; }
        public string RawPayDate { get; set; }
        public string CalculatedOutputDate { get; set; }
        public string RawCalculatedOutputDate { get; set; }
        
        //Julie defined new field
        public string EffectiveDate { get; set; }
        public string NextCouponDate { get; set; }
        public string PreviousCouponDate { get; set; }

        //Variable Fields 
        public string CurrentCPN { get; set; }
        public string LastRefixDate { get; set; }
        public string RawLastRefixDate { get; set; }
        public string LastRefixDatePlus31Days { get; set; }
        public string RawLastRefixDatePlus31Days { get; set; }
        public string Frequency { get; set; }
        //Other Fields
        public string ResetFrequency { get; set; }
        public string NextAccuralPeriodBeginning { get; set; }

    }