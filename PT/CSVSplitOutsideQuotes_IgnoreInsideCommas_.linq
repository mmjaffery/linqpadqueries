<Query Kind="Program" />

void Main()
{
	List<PI_PTTermSyncCredit> lastNightsCreditPaymentsList = new List<PI_PTTermSyncCredit>();
	
	string filePath = @"C:\06282017\1022\Credits_Applied_4AM_2019-10-22.csv";

	using (var fs = File.OpenRead(filePath))
	{
		using (var reader = new StreamReader(fs))
		{
			string headerLine = reader.ReadLine();
			string currentLine;

			while ((currentLine = reader.ReadLine()) != null)
			{
				//string[] strArray = currentLine.Split(',');
				Regex regx = new Regex(',' + "(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
				string[] line = regx.Split(currentLine);
				string[] items = line.Select(x =>
													x.Replace("\"", string.Empty)
													).ToArray();

				if (items.Length > 1 && items.Length == 7)
				{

					PI_PTTermSyncCredit creditObj = new PI_PTTermSyncCredit();

					creditObj.FileID = 1232;
					creditObj.CustomerName = items[0];
					creditObj.CustomerID = items[1];
					creditObj.CreditID = items[2];
					creditObj.CreditAppliedAmount = items[3];
					creditObj.CreditAppliedDate = items[4];
					creditObj.InvoiceNumber = items[5];
					creditObj.TermSyncID = items[6];

					lastNightsCreditPaymentsList.Add(creditObj);
				}
				else if (items.Length == 8)
				{
					Console.WriteLine(Environment.NewLine + "More Commas Found >> " + currentLine + Environment.NewLine);
				}
				else
				{
					File.AppendAllText(@"C:\06282017\1022\LogFile.txt", "More Commas in " + filePath + "" + DateTime.Now + Environment.NewLine);
				}
			}
		}
	}

	
}

// Define other methods and classes here

public class PI_PTTermSyncCredit
{
	public int PaymentID { get; set; }
	public int FileID { get; set; }
	public string CustomerName { get; set; }
	public string CustomerID { get; set; }
	public string CreditID { get; set; }
	public string CreditAppliedAmount { get; set; }
	public string CreditAppliedDate { get; set; }
	public string OriginalAmount { get; set; }
	public string InvoiceNumber { get; set; }
	public string TermSyncID { get; set; }
}