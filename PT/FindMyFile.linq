<Query Kind="Program" />

void Main()
{
	String strFileName = "Todays_Pending_Settled_Date_Credit_Card_11AM_20181130";
	String strCredits = "credits";
	String strPaid = "paid";
	String strPending = "pending";

	Console.WriteLine("Does '{0}' contain '{1}'?", strFileName, strCredits);
	
	strFileName = strFileName.ToLower();
	
	bool foundCreditFile = strFileName.Contains(strCredits);
	bool foundPaidFile = strFileName.Contains(strPaid);
	bool foundPendingFile = strFileName.Contains(strPending);
	
	Console.WriteLine ("Credit File {0}" , foundCreditFile);
	Console.WriteLine ("Paid File {0}" , foundPaidFile);
	Console.WriteLine ("Pending File {0}" , foundPendingFile);
	
	
	
//	StringComparison comp = StringComparison.Ordinal;
//	Console.WriteLine("   {0:G}: {1}", comp, strFileName.Contains(strCredits, comp));

	//comp = StringComparison.OrdinalIgnoreCase;
	//Console.WriteLine("   {0:G}: {1}", comp, s.Contains(sub1, comp));
}

// Define other methods and classes here