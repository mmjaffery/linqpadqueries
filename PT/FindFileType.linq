<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Collections.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.IO.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Net.WebHeaderCollection.dll</Reference>
</Query>

// This query will produce the full path for all .txt files  
// under the specified folder including subfolders.  
// It orders the list according to the file name.  
void Main()
{
	string startFolder = @"H:\Jaffery\PTTermSyncInbound\InBound\";

	// Take a snapshot of the file system
	DirectoryInfo dir = new DirectoryInfo(startFolder);

	// This method assumes that the application has discovery permissions  
	// for all folders under the specified path.  
	IEnumerable<FileInfo> fileList = dir.GetFiles("*.*", SearchOption.AllDirectories);

	System.Collections.Generic.IEnumerable<FileInfo> fileQuery = from file in fileList
																 where file.Extension == ".csv"
																 orderby file.Name 
																 select file;
																 
	foreach (var fi in fileQuery)
	{
		
		Console.WriteLine( fi.FullName + " _ " + File.GetLastWriteTime(fi.FullName).ToString());
		var array = File.ReadAllLines(fi.FullName)
			            .Select(f => f.Split(','))
						.Skip(1)
						.Select(s => int.Parse(s));
						
		Console.WriteLine(array.ToString());
			
			
			
		
	}
	
	


	//Console.WriteLine("\r\nThe newest .txt file is {0}. Creation time: {1}",
	//	  newestFile.FullName, newestFile.CreationTime);

	// Keep the console window open in debug mode.  
	Console.WriteLine("Press any key to exit");
	//Console.ReadKey();


	



}

// Define other methods and classes here