<Query Kind="Program" />

void Main()
{
	MyClass.WriteCount(3,15);
}

// Define other methods and classes here
static class MyClass
{
    public static void WriteCount(int min, int max)
    {
        int current = min;
        Action wc = () => (current++).Dump();

        while (current != max)
            wc();
    }
}