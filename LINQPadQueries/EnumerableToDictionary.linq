<Query Kind="Program">
  <Connection>
    <ID>c08ecca0-d46f-45e5-94f7-3b0731f26784</ID>
    <Persist>true</Persist>
    <Server>ISD</Server>
    <Database>invstorportal_data</Database>
    <ShowServer>true</ShowServer>
  </Connection>
</Query>

void Main()
{
	var details = (from row in BREntries
				   select new 
					{
						Name = (row.BREntryID + row.FundFamily + row.FundID), 
						row.Amount
					}).ToDictionary (d => d.Name, di=> di.Amount);
					
	details.Dump();
}
