<Query Kind="Program" />

void Main()
{
	string filepath = @"S:\DreamTeam\Mohammad";
	DirectoryInfo directory = new DirectoryInfo(filepath);
	//int fileCounter = 0;
	List<string> editedFileNameList = new List<string>();
	List<string> removedSpecialCharsList = new List<string>();

	foreach (var file in directory.GetFiles())
	{
		string originalName = file.Name;
		int dashIndexPosition = originalName.IndexOf('-') + 1;
		
		string changedName = originalName.Substring(dashIndexPosition);	
		editedFileNameList.Add(changedName);
		
		string removedSpecialCharacters = Regex.Replace(changedName, @"[^0-9a-zA-Z\._-]", string.Empty);
		removedSpecialCharsList.Add(removedSpecialCharacters);
		
		// + filepath + 
		
		Console.WriteLine("OLD > " + @"\" + originalName);
		Console.WriteLine("STAGE1 > " + @"\" + changedName);
		Console.WriteLine("STAGE2 > " + @"\" + removedSpecialCharacters);
		Console.WriteLine();
		
		//Actual File Replace Command is the File.Move.
		System.IO.File.Move(filepath + @"\" + originalName, filepath + @"\" + changedName);
	}

	editedFileNameList.Count().Dump();

	//Console.WriteLine(fileCounter + " files");

}

// Define other methods and classes here