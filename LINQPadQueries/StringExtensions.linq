<Query Kind="Program" />

void Main()
{
	   string myString = "This _, is a# test for you & Do` you know~ what I mean_ haha .".RemoveSpecialCharacters().Dump();  
}

// Define other methods and classes here
public static class StringExtensions
    {
		public static string RemoveSpecialCharacters(this string input)
		{
			return Regex.Replace(input, "[&,#'~`_.]+", "", RegexOptions.Compiled).Trim();
		}
		 public static bool IsWholeNumber(this string theValue)
        {
            long retNum;
            return long.TryParse(theValue, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
        }

        public static DateTime? ToDateTime(this string s)
        {
            DateTime dtr;
            var tryDtr = DateTime.TryParse(s, out dtr);
            return (tryDtr) ? dtr : new DateTime?();
        }

        public static bool IsValidChoice(this string choice)
        {
            return ((choice == "-1") || (choice == "")) ? false : true;
        }

        //http://www.codeproject.com/Articles/692603/Csharp-String-Extensions
        public static double ToDouble(this string input, bool throwExceptionIfFailed = false)
        {
            double result;
            var valid = double.TryParse(input, NumberStyles.AllowDecimalPoint,
              new NumberFormatInfo { NumberDecimalSeparator = "." }, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as double", input));
            return result;

        }

        public static decimal ToDecimal(this string val)
        {
            decimal d;
            if (!decimal.TryParse(val, out d))
            {
                d = 0.0M;
            }
            return d;
        }

        public static bool IsDecimal(this string val)
        {
            decimal result;
            return decimal.TryParse(val, out result);
        }

        public static bool IsPositiveDecimal(this string val)
        {
            decimal result;
            decimal.TryParse(val, out result);

            if (result > 0)
                return true;
            else
                return false;            
        }

        public static int ToInt(this string @string)
        {
            return int.Parse(@string);
        }

        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public static bool IsNullOrBlank(this String text)
        {
            return text == null || text.Trim().Length == 0;
        }

        public static bool IsPositiveInteger(this string s)
        {
            Regex regex = new Regex(@"^\d*$");
            return regex.IsMatch(s);
        }

	}