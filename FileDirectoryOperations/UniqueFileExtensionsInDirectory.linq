<Query Kind="Program" />

void Main()
{
	String filepath = @"C:\temp";
	DirectoryInfo directory = new DirectoryInfo(filepath);
	var fileList = from file in directory.GetFiles()
				   select file.Extension
				   ;

	Console.WriteLine(fileList.Count());

	HashSet<string> fileHashset = new HashSet<string>(fileList);
	fileHashset.Dump();

}

// Define other methods and classes here