<Query Kind="Program" />

void Main()
{
	string descartesFile = @"S:\DreamTeam\Mohammad\DescartesAutomation\Inbound\GdmToPartsTown_20190121230718.csv" ;
	string descartesArchiveFile = @"S:\DreamTeam\Mohammad\DescartesAutomation\Inbound\Archive\GdmToPartsTown_20190121230718.csv" ;
	string descartesArchiveFolder = @"S:\DreamTeam\Mohammad\DescartesAutomation\Inbound\Archive\";

	if (!File.Exists(descartesFile))
	{
		// This statement ensures that the file is created,
		// but the handle is not kept.
		using (FileStream fs = File.Create(descartesFile)) { }
	}

	
	if (File.Exists(descartesArchiveFile))
	{
		File.Delete(descartesArchiveFile);
		
		//MoveRetry(descartesFile, descartesArchiveFolder);
		//File.Delete(descartesFile);
	}
	
	MoveRetry(descartesFile, descartesArchiveFolder);
	Console.WriteLine("Copied!");
}

void MoveRetry(string descartesFile, string descartesArchiveFolder)
{
	int NumberOfRetries = 3;
	int DelayOnRetry = 1000;
	for (int i = 1; i <= NumberOfRetries; ++i)
	{
		try
		{
			if (File.Exists(descartesFile))
			File.Copy(descartesFile, descartesArchiveFolder);
			Console.WriteLine("Copied!");
			// Do stuff with file
			break; // When done we can break loop
		}
		catch (IOException e) when (i <= NumberOfRetries)
		{
			// You may check error code to filter some exceptions, not every error
			// can be recovered.
			Thread.Sleep(DelayOnRetry);
			Console.WriteLine("Retry {0}", i);
		}
	}
}

// Define other methods and classes here