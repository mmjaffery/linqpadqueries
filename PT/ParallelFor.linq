<Query Kind="Program">
  <Namespace>System</Namespace>
  <Namespace>System.Diagnostics</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	
	Console.WriteLine("Number of Threads: {0}", System.Diagnostics.Process.GetCurrentProcess().Threads.Count);
	Console.WriteLine("Number of Processors: {0}", Environment.ProcessorCount);
		Foo();
		//FooParallel();
		FooParallelFor(2);
		FooParallelFor(3);
		FooParallelFor(4);
		FooParallelFor(5);
		//Foo();
		
	

	//Console.WriteLine("Hello World");
	
}

void Foo()
{
	Stopwatch stopwatch = new Stopwatch();
	stopwatch.Start();
	for (int j = 0; j < 9999999; j++)
	{		
		RandomString(10);		
	};
	stopwatch.Stop();
	Console.WriteLine(stopwatch.ElapsedMilliseconds + " ElapsedMilliseconds");

}

void FooParallelFor(int threadMax)
{
	//
	//
	Stopwatch stopwatch = new Stopwatch();
	stopwatch.Start();
	Parallel.For(0, 9999999, new ParallelOptions { MaxDegreeOfParallelism = threadMax}, (i, state) =>
	 {
		 RandomString(10);
	 });

	stopwatch.Stop();
	Console.WriteLine("Threads Used > " + threadMax  + " = " + stopwatch.ElapsedMilliseconds + " ElapsedMilliseconds");
	

}

void FooParallel()
{
	Console.WriteLine(Environment.ProcessorCount);
	Parallel.For(
		0
		, 100000
		,new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount } 
		, (i) => {
		RandomString(10);
		});
	
}

private static string RandomString(int length)
{
	const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	var random = new Random();
	return new string(Enumerable.Repeat(chars, length)
	  .Select(s => s[random.Next(s.Length)]).ToArray());
}