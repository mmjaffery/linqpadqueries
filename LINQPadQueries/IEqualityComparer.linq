<Query Kind="Program" />

void Main()
{

	var withDupes = CreateTickerList().AsEnumerable().Distinct(new DistinctItemComparer());
	HashSet<TickerCategory> noDupes = new HashSet<TickerCategory>(withDupes);	
	noDupes.Dump();
}

public class TickerCategory
    {
        public int TKR { get; set; }
        public string SECCAT { get; set; }
    }
	
public class DistinctItemComparer : IEqualityComparer<TickerCategory>
    {
        public bool Equals(TickerCategory x, TickerCategory y)
        {
            return x.TKR == y.TKR &&
                   x.SECCAT == y.SECCAT; 
        }

        public int GetHashCode(TickerCategory obj)
        {
            return obj.TKR.GetHashCode();
        }
    }
		
 public List<TickerCategory> CreateTickerList()
        {
            return new List<TickerCategory>()
            {
                new TickerCategory{ TKR = 1, SECCAT = "EQUITY1"}, 
                new TickerCategory{ TKR = 2, SECCAT = "EQUITY2"},
				new TickerCategory{ TKR = 3, SECCAT = "EQUITY3"}, 
				new TickerCategory{ TKR = 3, SECCAT = "EQUITY3"}, 
				new TickerCategory{ TKR = 4, SECCAT = "EQUITY4"}, 
				new TickerCategory{ TKR = 5, SECCAT = "EQUITY5"}, 
				new TickerCategory{ TKR = 5, SECCAT = "EQUITY5"}, 
				
			};
		}

// Define other methods and classes here