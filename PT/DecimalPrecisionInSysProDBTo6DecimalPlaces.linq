<Query Kind="Program" />

void Main()
{
	
	//decimal x = 2;
	//Console.WriteLine(Math.Round(x, 6));
	//Console.WriteLine(Math.Round(x, 4));
	//Console.WriteLine(Decimal.Parse(x.ToString("0.000000")));
	
	//decimal a = 18M;
	//decimal b = Math.Round(a, 6);
	
	//Console.WriteLine(a);
	//Console.WriteLine(b);
	//read more about it @
	//http://stackoverflow.com/questions/27269607/c-sharp-decimal-how-to-add-trailing-zeros#27289497
	

	decimal value = 1M;
	decimal withPrecision = (value * 1.000000M) + 0.000000M;
	
	Console.WriteLine(value);
	Console.WriteLine(withPrecision);
	
}

// Define other methods and classes here
