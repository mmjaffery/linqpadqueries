<Query Kind="Program">
  <Connection>
    <ID>c08ecca0-d46f-45e5-94f7-3b0731f26784</ID>
    <Persist>true</Persist>
    <Server>ISD</Server>
    <Database>NewPortal</Database>
    <ShowServer>true</ShowServer>
  </Connection>
</Query>

void Main()
{
	var stuff = from l in File.ReadAllLines("T:\\ApplicationsDev\\WFANewPortal\\Carriers.CSV").Skip(1)
            let columns = l.Split(',')
			select new CarrierInformation
			{				
               	CompanyName = columns[0].ToString(), 
				Email = columns[1].Substring( columns[1].IndexOf('@'), columns[1].Length - columns[1].IndexOf('@') ), 
				Type = columns[2]
            };
			
		//stuff = stuff.ToList();
		
		//Alternative 1
		//Insert All together
		CarrierInformation.InsertAllOnSubmit<CarrierInformation>(stuff);
		SubmitChanges();
		
		
		//Aternative 2 = Works
		//Object by Object - hence the looping 
//		foreach (var row in	stuff)
//		{
//			CarrierInformation newCarrier = new CarrierInformation()
//			{ 
//				ID = 1, CompanyName = row.CompanyName , Email = row.Email, Type =  row.Type
//			};			
//			CarrierInformation.InsertOnSubmit(newCarrier);				
//		}		
//		SubmitChanges();
		
		
		
			
}

// Define other methods and classes here