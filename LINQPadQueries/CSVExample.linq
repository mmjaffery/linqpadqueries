<Query Kind="Program">
  <Connection>
    <ID>c08ecca0-d46f-45e5-94f7-3b0731f26784</ID>
    <Persist>true</Persist>
    <Server>ISD</Server>
    <Database>invstorportal_data</Database>
    <ShowServer>true</ShowServer>
  </Connection>
  <Reference>&lt;RuntimeDirectory&gt;\System.Text.Encoding.dll</Reference>
</Query>

void Main()
{
	var allLines = (from row in BREntries
					select row);
	var allLinesList = allLines.ToList();
	
	var csv = new StringBuilder();
	
	foreach (var item in allLines)
	{
		csv.AppendLine(string.Join(",", item));		
	}
	
	File.WriteAllText("C:\\BCP\\Test.csv", csv.ToString());
}

// Define other methods and classes here
