<Query Kind="Program" />

void Main()
{
	int custNo = 6670098;
	
	//method 1 
	//ToSYSPROCustomerNumberFormat
	string convertedCustNo = custNo.ToString("000000000000000");
	Console.WriteLine(convertedCustNo);

	//method 2
	string convertedCustNo2 = custNo.ToString("D15");
	Console.WriteLine(convertedCustNo2);


}

// Define other methods and classes here