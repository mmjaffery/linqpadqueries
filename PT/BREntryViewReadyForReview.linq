<Query Kind="Program">
  <Connection>
    <ID>c08ecca0-d46f-45e5-94f7-3b0731f26784</ID>
    <Persist>true</Persist>
    <Server>ISD</Server>
    <Database>invstorportal_data</Database>
    <ShowServer>true</ShowServer>
  </Connection>
</Query>

void Main()
{
	BREntryView.Where (	e => e.FundFamily == "DEMOFUND" && e.FundID == "DEMOLTDII" 
	&& ((e.StatusBy!="WOODFIELD\\mjaffery" && e.Status =="ReadyForReview") || 
	(e.AddedBy=="WOODFIELD\\mjaffery" && e.Status=="Initiated") || 
	(e.StatusBy=="WOODFIELD\\mjaffery" && e.Status=="LockedForReview")) ).ToList().Dump();
}

// Define other methods and classes here