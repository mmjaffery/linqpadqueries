<Query Kind="Program" />

void Main()
{
	int a = 3;
	int b = 4;
	
	a.Dump();
	b.Dump();
	
	
	MyClass.Swap(ref a, ref b);
	
	a.Dump();
	b.Dump();
}

// Define other methods and classes here
class MyClass
{
    public static void Swap<T>(ref T l, ref T r)
    {
        T temp = r;
        r = l;
        l = temp;
    }
}