<Query Kind="Program" />

void Main()
{
  	List<PrivateExchangeTicker> TickerList = new List<PrivateExchangeTicker>{
	new PrivateExchangeTicker { ReturnCode = "UP", Ticker = "IWM" },
	new PrivateExchangeTicker { ReturnCode = "CT", Ticker = "FVI CN" },
	new PrivateExchangeTicker { ReturnCode = "CV", Ticker = "MVN CN" }	
	};
	
	
	string longTickerCode = "FVI CN Equity";
	//string longTickerCode2 = "MVN CN Equity";
	
	OlderTechnique(longTickerCode, TickerList).Dump();
	NewerTechnique(longTickerCode, TickerList).Dump();
	
}

public string NewerTechnique(string longTickerCode, List<PrivateExchangeTicker> TickerList)
{
	string[] arr = longTickerCode.Split(' ');
	
	var search = (from item in TickerList
				 where item.Ticker.StartsWith(arr[0])
				 select item);
				 
	var returnable = search.FirstOrDefault().Ticker;
				 
	search.Dump();
				 
	
	
	return "The found string using Newer is " + returnable;

}

public string OlderTechnique(string longTickerCode, List<PrivateExchangeTicker> TickerList)
{
	string[] arr = longTickerCode.Split(' ');
    string partialTickerCode = arr[0] + " " + arr[1];

    var search = (from item in TickerList
                  where item.Ticker == partialTickerCode
                  select item.Ticker).FirstOrDefault();

    return "The found string using older is = "+ search ;
}
// Define other methods and classes here
public class PrivateExchangeTicker
    {
        public string ReturnCode { get; set; }
        public string Ticker { get; set; }

        public PrivateExchangeTicker()
        {

        }

        public PrivateExchangeTicker(string privateExchangeTicker, string tickerCode)
        {
            this.ReturnCode = privateExchangeTicker;
            this.Ticker = tickerCode;
        }

        public override string ToString()
        {
            return string.Format("{0}, : {1}", ReturnCode, Ticker);
        }

    }