<Query Kind="Program">
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
		Parallel.For(0, 30, (i) =>
		{
			Console.WriteLine("My i value is " + i);
		});
		
		Console.WriteLine("Press any key to continue...");
		
}

// Define other methods and classes here