<Query Kind="Program" />

void Main()
{
	using (System.IO.StreamReader sr = new System.IO.StreamReader(@"S:\DreamTeam\Jobs\AdHoc\CustomerCreationTemplate\CPSMassLoad.csv"))
	{
		int fileNumber = 0;

		while (!sr.EndOfStream)
		{
			int count = 0;

			using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"S:\DreamTeam\Jobs\AdHoc\CustomerCreationTemplate\CPS_MassLoad_" + ++fileNumber + ".csv"))
			{
				sw.AutoFlush = true;

				while (!sr.EndOfStream && ++count < 200)
				{
					sw.WriteLine(sr.ReadLine());
				}
			}
		}
	}
}