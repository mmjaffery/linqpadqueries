<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
</Query>

void Main()
{
	Movie movie = new Movie
	{
		Name = "Bad Boys",
		Year = 1995
	};


	// serialize JSON to a string and then write string to a file
	File.WriteAllText(@"C:\06282017\movie.json", JsonConvert.SerializeObject(movie));

	// serialize JSON directly to a file
	using (StreamWriter file = File.CreateText(@"C:\06282017\movie.json"))
	{
		JsonSerializer serializer = new JsonSerializer();
		serializer.Serialize(file, movie);
	
	
	}
	
	
}

// Define other methods and classes here
public class Movie
{
	public string Name { get; set; }
	public int Year { get; set; }
}