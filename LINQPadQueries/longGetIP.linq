<Query Kind="Program">
  <Namespace>System.Net</Namespace>
</Query>

void Main()
{
	//Solution 1:
	
	string myExternalIP;
	string strHostName = System.Net.Dns.GetHostName();
	string clientIPAddress = System.Net.Dns.GetHostAddresses(strHostName).GetValue(0).ToString();
	string clientip = clientIPAddress.ToString();
	System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create("http://www.ipaddress.com/");
	request.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE" +
	"6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
	System.Net.HttpWebResponse response =
	(System.Net.HttpWebResponse)request.GetResponse();
	using (System.IO.StreamReader reader = new StreamReader(response.GetResponseStream()))
	{
		myExternalIP = reader.ReadToEnd();
		reader.Close();
	}
	
	myExternalIP.Dump();
	
	string[] str = Regex.Split(myExternalIP,"IP:");
	string theip = str[1].Substring(0,14);
	string ip = theip; // Then you can use it in the view through the Viewbag
	ip.Dump();
}

// Define other methods and classes here
