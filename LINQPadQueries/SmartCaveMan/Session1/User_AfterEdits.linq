<Query Kind="Program">
  <Namespace>System</Namespace>
  <Namespace>System.Collections.Generic</Namespace>
  <Namespace>System.Linq</Namespace>
  <Namespace>System.Text</Namespace>
</Query>

void Main()
{
}


    //Seralizing this class 
    //and storing in the FormsAuthentication Ticket.
    //what becomes the userData going into the Forms Authentication Ticket.
    //will eventually be serialized
    public class User 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string AccessCode { get; set; }
    }

    class AuthenticationTicketData
    {
        public User User { get; set; }
        public InviteInfo Invitation { get; set; }
    }

    public class InviteInfo 
    {
        public string Email { get; set;}
        public string AccessCode{get;set;}
    }


