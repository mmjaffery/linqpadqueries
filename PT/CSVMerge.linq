<Query Kind="Program" />

void Main()
	{
		IEnumerable<string> allCsv = Directory.EnumerateFiles(@"C:\06282017", "*.*", SearchOption.TopDirectoryOnly);
		String[]  header= { File.ReadLines(allCsv.First()).First(l => !string.IsNullOrWhiteSpace(l)) };
		var mergedData = allCsv
			.SelectMany(csv => File.ReadLines(csv)
				.SkipWhile(l => String.IsNullOrWhiteSpace(l)).Skip(1)); // skip header of each file
		
		File.WriteAllLines(@"C:\06282017\Query.csv", header.Concat(mergedData));
		
	}


// Define other methods and classes here