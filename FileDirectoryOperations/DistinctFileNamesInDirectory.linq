<Query Kind="Program" />

void Main()
{
	string filepath = @"C:\tempUntouched";
	DirectoryInfo directory = new DirectoryInfo(filepath);
	
	var fileNameList = from file in directory.GetFiles()
					   select file.Name;
					   
	Console.WriteLine("Total List Count = " + fileNameList.Count());

	var fileNameDistinctList = fileNameList.Distinct().ToList();
	Console.WriteLine("Distinct List Count = " + fileNameDistinctList.Count());
	
	//foreach (var file in fileNameDistinctList)
	//{
	//	Console.WriteLine(file);
	//}
}

// Define other methods and classes here