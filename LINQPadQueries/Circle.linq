<Query Kind="Program" />

void Main()
{
	Circle.GetCircleArea(2).Dump();
	Circle.GetCircleLength(3).Dump();
	Circle.GetCircleDiameter(36).Dump();
	
	
	
}

public static class Circle
	{
		public static double GetCircleLength(double Diameter)
		{
			return Diameter * Math.PI;
		}
	
		public static double GetCircleArea(double Radius)
		{
			return Math.Pow(Radius, 2) * Math.PI;
		}
	
		public static double GetCircleDiameter(double Area)
		{
			return Math.Sqrt((Area / Math.PI));
		}
	}
