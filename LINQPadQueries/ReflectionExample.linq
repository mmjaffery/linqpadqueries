<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Windows.Forms.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Security.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Configuration.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\Accessibility.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Deployment.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Runtime.Serialization.Formatters.Soap.dll</Reference>
  <Namespace>System.Windows.Forms</Namespace>
  <Namespace>System.Windows.Forms.VisualStyles</Namespace>
</Query>

void Main()
{
	
}

//http://www.codeproject.com/Articles/1094625/Advanced-programming-with-Csharp-Lecture-Notes-Par
class Document
{
    //A (static) key-value dictionary to store string - constructor information.
    static Dictionary<string, ConstructorInfo> specialized;

    public static HTMLElement CreateElement(string tag)
    {
        //Has the key-value dictionary been initialized yet? If not ...
        if (specialized == null)
        {
            //Get all types from the current assembly (that includes those HTMLElement types)
            var types = Assembly.GetCallingAssembly().GetTypes();

            //Go over all types
            foreach(var type in types)
            {
                //If the current type is derived from HTMLElement
                if (type.IsDerivedFrom(typeof(HTMLElement)))
                {
                    //Get the constructor of the type - with no parameter
                    var ctor = type.GetConstructor(Type.Empty);

                    //If there is an empty constructor (otherwise we do not know how to create an object)
                    if (ctor != null)
                    {
                        //Call that constructor and treat it as an HTMLElement
                        var element = ctor.Invoke(null) as HTMLElement;

                        //If all this succeeded add a new entry to the dictionary using the constructor and the tag
                        if (element != null)
                            specialized.Add(element.Tag, ctor);
                    }
                }
            }
        }

        //If the given tag is available in the dictionary then call the stored constructor to create a new instance
        if (specialized.ContainsKey(tag))
            return specialized[tag].Invoke(null) as HTMLElement;

        //Otherwise this is an object without a special implementation; we know how to handle this!
        return new HTMLElement(tag);
    }
}

// Define other methods and classes here
class HTMLElement
	{
	    string _tag;
	
	    public HTMLElement(string tag)
	    {
	        _tag = tag;
	    }
	
	    public string Tag
	    {
	        get { return _tag; }
	    }
	}
	
	class HTMLImageElement : HTMLElement
	{
	    public HTMLImageElement() : base("img")
	    {
	    }
	}
	
	class HTMLParagraphElement : HTMLElement
	{
	    public HTMLParagraphElement() : base("p")
	    {
	    }
	}