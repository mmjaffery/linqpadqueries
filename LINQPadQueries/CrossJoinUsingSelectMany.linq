<Query Kind="Program" />

void Main()
{
  List<string> animals = new List<string>() { "cat", "dog", "donkey" };
  List<int> number = new List<int>() { 10, 20 };

  var mix=number.SelectMany(num => animals, (n, a) => new { n, a }) .Dump();
}

// Define other methods and classes here
