<Query Kind="Program" />

void Main()
{
	var csvData = from row in MyExtensions.ReadFrom(@"T:\ApplicationsDev\WFANewPortal\Carriers.CSV").Skip(1)
				  let columns = row.Split(',')
				  select new 
				  {
				  	CompanyName = columns[0], 
					Email = columns[1], 
					Type = columns[2]					
				  };
				  
	csvData.Dump();
	
}



