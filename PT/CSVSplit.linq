<Query Kind="Program" />

void Main()
{
	int index = 0;
	var groups = from line in File.ReadLines(@"C:\06282017\Products_Prop65Warnings_012519_webcorrection.csv")
				 group line by index++ / 100000 into g
				 select g.AsEnumerable();
	int file = 0;
	foreach (var group in groups)
	{		
		File.WriteAllLines((@"C:\06282017\Products_Prop65Warnings_012519_webcorrection__" + file++ + ".csv").ToString(), group.ToArray());
	}
}

// Define other methods and classes here