<Query Kind="Program" />

void Main()
{
	string filepath = @"C:\temp";
	DirectoryInfo directory = new DirectoryInfo(filepath);
	//int fileCounter = 0;
	List<string> editedFileNameList = new List<string>();

	foreach (var file in directory.GetFiles())
	{
		string originalName = file.Name;
		int dashIndexPosition = originalName.IndexOf('-') + 1;
		string changedName = originalName.Substring(dashIndexPosition);
		editedFileNameList.Add(changedName);
		
		//Console.WriteLine("OLD > " + filepath + @"\" + originalName);
		//Console.WriteLine("NEW > " + filepath + @"\" + changedName);
		//Actual File Replace Command is the File.Move.
		//System.IO.File.Move(filepath + @"\" + originalName, filepath + @"\" + changedName);
	}

	editedFileNameList.Count().Dump();
	editedFileNameList.Distinct().Count().Dump();
	
	
	int filesInQuestion = editedFileNameList.Count() - editedFileNameList.Distinct().Count();
	("files in question = " + filesInQuestion).Dump();
	
	HashSet<string> fileNameHashset = new HashSet<string>(editedFileNameList);
	fileNameHashset.Count().Dump();

	//Console.WriteLine(fileCounter + " files");

}

// Define other methods and classes here