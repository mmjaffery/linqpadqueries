<Query Kind="Program" />

void Main()
{
	string str_CR_Check = "_CR00001";
	//string str_inv_Check = "22463383";

	//Only letters:
	Console.WriteLine(str_CR_Check + " has Only letters "  +  Regex.IsMatch(str_CR_Check, @"^[a-zA-Z]+$"));
	
	//Only letters and numbers:
	Console.WriteLine(str_CR_Check + " has Only letters and numbers "  +  Regex.IsMatch(str_CR_Check, @"^[a-zA-Z0-9]+$"));
	
	//Only letters, numbers and underscore:
	Console.WriteLine(str_CR_Check + " has Only letters, numbers and underscore "  +  Regex.IsMatch(str_CR_Check, @"^[a-zA-Z0-9_]+$"));
}

// Define other methods and classes here
