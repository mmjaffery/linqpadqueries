<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Drawing.dll</Reference>
  <Namespace>System.Drawing</Namespace>
</Query>

void Main()
{
	GetRandomColour().Dump();
}

public static readonly Random rand = new Random();
public Color GetRandomColour()
{
    return Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256));
}
