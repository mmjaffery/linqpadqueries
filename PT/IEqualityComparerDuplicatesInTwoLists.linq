<Query Kind="Program" />

void Main()
{
	var yesterdaysList = CreateYesterdaysList();
	var todaysList = CreateTodaysList();

	Console.WriteLine("Duplicates Here :" + Environment.NewLine);
	Console.WriteLine("-------------------------------------" + Environment.NewLine);
	IEnumerable<StockCodeStatus> duplicatesInList = yesterdaysList.Intersect(todaysList, new DuplicateItemComparer());
	foreach (var element in duplicatesInList)
	{
		Console.WriteLine(element.StockCode + " , " + element.StockStatus);

	}
	Console.WriteLine("-------------------------------------" + Environment.NewLine);

	Console.WriteLine("Non-Matching Here :" + Environment.NewLine);
	Console.WriteLine("-------------------------------------" + Environment.NewLine);
	IEnumerable<StockCodeStatus> nonMatchingInList = yesterdaysList.Intersect(todaysList, new NonMatchingStatusComparer());
	foreach (var element in nonMatchingInList)
	{
		Console.WriteLine(element.StockCode + " , " + element.StockStatus);
	}
}



public class StockCodeStatus
{
	public string StockCode { get; set; }
	public string StockStatus { get; set; }
}

public class DuplicateItemComparer : IEqualityComparer<StockCodeStatus>
{
	public bool Equals(StockCodeStatus x, StockCodeStatus y)
	{
		return x.StockCode == y.StockCode &&
			   x.StockStatus == y.StockStatus;
	}

	public int GetHashCode(StockCodeStatus obj)
	{
		return obj.StockCode.GetHashCode();
	}
}


public class NonMatchingStatusComparer : IEqualityComparer<StockCodeStatus>
{
	public bool Equals(StockCodeStatus x, StockCodeStatus y)
	{
		return x.StockCode == y.StockCode &&
			   x.StockStatus != y.StockStatus;
	}

	public int GetHashCode(StockCodeStatus obj)
	{
		return obj.StockCode.GetHashCode();
	}
}


public List<StockCodeStatus> CreateYesterdaysList()
{
	return new List<StockCodeStatus>()
	{
	 	new StockCodeStatus{ StockCode = "APW2425400", StockStatus="StockedActive" },
		new StockCodeStatus{ StockCode = "APW2425500", StockStatus="StockedActive" },
		new StockCodeStatus{ StockCode = "APW25000017", StockStatus="StockedActive" },
		new StockCodeStatus{ StockCode = "APW25000080", StockStatus="StockedActive" },
		new StockCodeStatus{ StockCode = "APW25000165", StockStatus="StockedActive" },
		new StockCodeStatus{ StockCode = "APW25001040", StockStatus="Discontinued" },
		new StockCodeStatus{ StockCode = "APW2971000", StockStatus="NonStockedActive" },
		new StockCodeStatus{ StockCode = "APW300228", StockStatus="NonStockedActive" },
		new StockCodeStatus{ StockCode = "APW30061031", StockStatus="Discontinued" },
		new StockCodeStatus{ StockCode = "APW30070241", StockStatus="NonStockedActive" },
		new StockCodeStatus{ StockCode = "APW30070242", StockStatus="StockedInActive" },
		new StockCodeStatus{ StockCode = "APW301052", StockStatus="StockedInActive" },
		new StockCodeStatus{ StockCode = "APW3015000", StockStatus="StockedInActive" }

	};
}


public List<StockCodeStatus> CreateTodaysList()
{
	return new List<StockCodeStatus>()
	{
	 	new StockCodeStatus{ StockCode = "APW2425400", StockStatus="StockedActive" },
		new StockCodeStatus{ StockCode = "APW2425500", StockStatus="StockedActive" },
		new StockCodeStatus{ StockCode = "APW25000017", StockStatus="StockedActive" },
		new StockCodeStatus{ StockCode = "APW25000080", StockStatus="StockedActive" },
		new StockCodeStatus{ StockCode = "APW25000165", StockStatus="Discontinued" },
		new StockCodeStatus{ StockCode = "APW25001040", StockStatus="Discontinued" },
		new StockCodeStatus{ StockCode = "APW2971000", StockStatus="NonStockedActive" },
		new StockCodeStatus{ StockCode = "APW300228", StockStatus="NonStockedActive" },
		new StockCodeStatus{ StockCode = "APW30061031", StockStatus="Discontinued" },
		new StockCodeStatus{ StockCode = "APW30070241", StockStatus="NonStockedActive" },
		new StockCodeStatus{ StockCode = "APW30070242", StockStatus="StockedInActive" },
		new StockCodeStatus{ StockCode = "APW301052", StockStatus="StockedInActive" },
		new StockCodeStatus{ StockCode = "APW3015000", StockStatus="StockedInActive" }

	};
}