<Query Kind="Program" />

private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
		
//https://stackoverflow.com/questions/1654887/random-next-returns-always-the-same-values		
		
static void Main()            
{
	for (int i = 0; i < 20; i++)
	{
		Console.WriteLine(RandomString(10));    
		Thread.Sleep(100);
	}         
	
}