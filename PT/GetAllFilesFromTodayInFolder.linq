<Query Kind="Program" />

void Main()
{
	string ReceiveLocation = @"S:\DreamTeam\Mohammad\TermsSync\Inbound";
	DirectoryInfo di = new DirectoryInfo(ReceiveLocation);
	FileInfo[] files = di.GetFiles("*.csv");
	List<TSFileInfo> listOfTSFileInfo = new List<TSFileInfo>();

	foreach (var file in files)
	{
		DateTime fileCreationTime = file.LastWriteTime;

		if (fileCreationTime.Date == DateTime.Now.Date)
		{
			var lines = File.ReadAllLines(ReceiveLocation + "\\" + file.Name);

			listOfTSFileInfo.Add(new TSFileInfo
			{
				FileName = file.Name,
				TotalRowCount = lines.Length
			}
			);
		}

	}
	
	listOfTSFileInfo.Dump();
}


public class TSFileInfo
{
	public int FileID { get; set; }
	public string FileName { get; set; }
	public int TotalRowCount { get; set; }

}
//