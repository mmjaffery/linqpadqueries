<Query Kind="Program" />

void Main()
{

	string filepath = @"C:\temp";
	DirectoryInfo directory = new DirectoryInfo(filepath);
	int fileCounter = 0;
	try
	{
		foreach (var file in directory.GetFiles())
		{
			//Find special character in the filename 
			//int tildaIndex = originalName.IndexOf("-") + 1;
			//if (tildaIndex >= 1)
			//	Console.WriteLine(originalName + " Found @ " + tildaIndex);
			//fileCounter = fileCounter + 1;
			//Console.WriteLine (originalName);

			string originalName = file.Name;
			int dashIndexPosition = originalName.IndexOf('-') + 1;
			string changedName = originalName.Substring(dashIndexPosition);
			Console.WriteLine("OLD > " + filepath + @"\" + originalName);
			Console.WriteLine("NEW > " + filepath + @"\" + changedName);
			//Actual File Replace Command is the File.Move.
			System.IO.File.Move(filepath + @"\" + originalName, filepath + @"\" + changedName);
		}

	}
	catch (IOException ex)
	{
		Console.WriteLine(ex.Message + "IO Exception!");	
	}
	Console.WriteLine(fileCounter + " files");
}

// Define other methods and classes here