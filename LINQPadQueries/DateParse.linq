<Query Kind="Program" />

void Main()
{
	string myDate = "20160630";
	DateTime? fileDate = (myDate.Substring(0,4) + "/" + myDate.Substring(4,2) + "/" + myDate.Substring(6,2)).ToDateTime();
	DateTime? otherFileDate = (myDate.Substring(0,4) + myDate.Substring(4,2) + myDate.Substring(6,2)).ToDateTime();
	otherFileDate.Dump();
	fileDate.Dump();
	 
}

// Define other methods and classes here
 public static class StringExtensions
    {
        public static bool IsNumeric(this string theValue)
        {
            long retNum;
            return long.TryParse(theValue, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
        }

        public static DateTime? ToDateTime(this string s)
        {
            DateTime dtr;
            var tryDtr = DateTime.TryParse(s, out dtr);
            return (tryDtr) ? dtr : new DateTime?();
        }
	}