<Query Kind="Program">
  <Namespace>System</Namespace>
  <Namespace>System.Collections.Generic</Namespace>
  <Namespace>System.Linq</Namespace>
  <Namespace>System.Security.Principal</Namespace>
  <Namespace>System.Text</Namespace>
</Query>

void Main()
{
}


    //Custom Principal object to be used in conjunction with
    //HttpContext.User.Identity 
    public class MyPrincipal : IPrincipal
    {
        public MyPrincipal()
        {
            AuthenticationTicketData = new AuthenticationTicketData();
        }

        public IIdentity Identity { 
            get { return this.AuthenticationTicketData; } }
        
        public AuthenticationTicketData AuthenticationTicketData { get; private set; }

        public User User { 
            get { return this.AuthenticationTicketData.User;}
            set { this.AuthenticationTicketData.User = value; }
        }

        public bool IsInRole(string role)
        {
            return true;
        }



    }
