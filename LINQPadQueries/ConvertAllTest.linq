<Query Kind="Program" />

void Main()
{
		var fieldList = new List<string> { "some string", " 2 ", "3 Thr ee", "4 four" };
		Example1(fieldList).Dump();
		//http://stackoverflow.com/questions/1948933/c-sharp-listt-convertall-in-net-2-0
		
		
		Example1Readable(fieldList).Dump();
}

private List<string> Example1( List<string> fieldList )
{
	
	fieldList = fieldList.ConvertAll<string>(new Converter<string, string>(
		delegate(string str)
		{
			str = str.Trim();
			str = str.Replace(' ', '_');
			return str;
		}
	));
	
	return fieldList;
}

private List<string> Example1Readable( List<string> fieldList )
{
	List<string> convertedFieldList = fieldList.ConvertAll<string>(new Converter<string, string>(
											delegate(string str)
											{
												str = str.Trim();
												str = str.Replace(' ', '_');
												return str;
											}
										));
										
	return convertedFieldList;
		
												

}

