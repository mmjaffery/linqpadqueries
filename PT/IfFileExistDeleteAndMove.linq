<Query Kind="Program" />

void Main()
{
	string descartesFile = @"S:\DreamTeam\Mohammad\DescartesAutomation\Inbound\GdmToPartsTown_20190121230718.csv";
	string descartesArchiveFile = @"S:\DreamTeam\Mohammad\DescartesAutomation\Inbound\Archive\GdmToPartsTown_20190121230718.csv";
	string descartesArchiveFolder = @"S:\DreamTeam\Mohammad\DescartesAutomation\Inbound\Archive\";
	string descartesFileName = "GdmToPartsTown_20190121230718.csv";

	if (File.Exists(descartesArchiveFolder + "Archive_" + descartesFileName))
	{
		File.Delete(descartesArchiveFolder + "Archive_" + descartesFileName);
	}
	File.Move(descartesFile, descartesArchiveFolder + "Archive_" + descartesFileName);
	Console.WriteLine("Copied!");
}

// Define other methods and classes here
