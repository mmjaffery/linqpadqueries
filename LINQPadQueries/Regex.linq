<Query Kind="Program" />

void Main()
{
	   string myString = "This _, is a# test for you & Do` you know~ what I mean_ haha .".RemoveSpecialCharacters().Dump();  
}

// Define other methods and classes here
public static class StringExtensions
    {
		public static string RemoveSpecialCharacters(this string input)
		{
			return Regex.Replace(input, "[&,#'~`_.]+", "", RegexOptions.Compiled).Trim();
		}
	}