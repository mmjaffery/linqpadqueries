<Query Kind="Program">
  <Namespace>System.Runtime.CompilerServices</Namespace>
</Query>

void Main()
{
	IEnumerable<string> options = new []{ "red", "green", "blue" };
	
	Tuple<StrongBox<System.String>,ICollection<System.String>>
										
										myOptions = 
											OptionSetFactory.ValueOptions<System.String> (options, "green");
											
	myOptions.Dump("options before changing");
											
	myOptions.Item1.Value = myOptions.Item2.Skip(2).First();
	
	myOptions.Dump("after changing");
	
	View(myOptions);
}

public static class OptionSetFactory
{
    public static Tuple<StrongBox<T>, ICollection<T>> ValueOptions<T>(IEnumerable<T> source, T selected){
      var optionset = Tuple.Create(new StrongBox<T>(), (ICollection<T>)new List<T>());
      if(source != null){
        foreach(var item in source){
          optionset.Item2.Add(item);
        }
      }
      if(selected != null)
      {
          optionset.Item1.Value = selected;
          if(!optionset.Item2.Contains(selected)){
            optionset.Item2.Add(selected);
          }
      }
      return optionset;
    }
    
  
}

public void View(Tuple<StrongBox<string>, ICollection<string>> viewModel){
	var selection = viewModel.Item1;
	var options = viewModel.Item2;
	var sb = new StringBuilder();
	sb.AppendLine("<select name=\"my-options\">");
	foreach(var option in options){
		sb.AppendLine("	<option value=\""+option+"\">" + option + "</option>");
	} 
	sb.AppendLine("</select>");
	Util.RawHtml(sb.ToString()).Dump();
}