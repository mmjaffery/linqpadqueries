<Query Kind="Program" />

static void Main()
{
	//Callback is an event
    Application.callback += () => Console.WriteLine("Number hit");
    Application.Run();
}

static class Application
{
    public static Action callback;
    
    public static void Run()
    {
        Random r = new Random(14);
    
        while (true)
        {
            double p = r.NextDouble();
            
            if (p < 0.0001 && callback != null)
                callback();
            else if (p > 0.9999)
                break;
        }
    }
}

//http://www.codeproject.com/Articles/1094625/Advanced-programming-with-Csharp-Lecture-Notes-Par