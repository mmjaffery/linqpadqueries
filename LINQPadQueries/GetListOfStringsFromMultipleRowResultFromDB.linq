<Query Kind="Program">
  <Connection>
    <ID>c08ecca0-d46f-45e5-94f7-3b0731f26784</ID>
    <Persist>true</Persist>
    <Server>ISD</Server>
    <Database>invstorportal_data</Database>
    <ShowServer>true</ShowServer>
  </Connection>
</Query>

void Main()
{
	var attachments = WorkPaperAttachments.Where (w => w.WorkSheetID == 477)
					    .Select (w => w.AttachedDocument);
	//attachments.Dump();
	
	string joint = string.Join("|", attachments);
	joint.Dump();
}

// Define other methods and classes here