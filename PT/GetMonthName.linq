<Query Kind="Program">
  <Namespace>System.Globalization</Namespace>
</Query>

void Main()
{
	
	//https://stackoverflow.com/questions/8711390/get-the-complete-month-name-in-english#8711409
	
	//Method 1
	CultureInfo ci = new CultureInfo("en-US");
	Console.WriteLine( DateTime.Now.ToString("MMMM", ci) );

	//Method 2
	// alternatively you can use CultureInfo.InvariantCulture:
	Console.WriteLine( DateTime.Now.ToString("MMMM", CultureInfo.InvariantCulture));

	//Method 3 - Next Month
	int month = DateTime.Now.Month + 1;
	// Or use CultureInfo.InvariantCulture if you want
	CultureInfo usEnglish = new CultureInfo("en-US");
	DateTimeFormatInfo englishInfo = usEnglish.DateTimeFormat;
	string monthName = englishInfo.MonthNames[month - 1];
	
	Console.WriteLine(monthName);
}

// Define other methods and classes here