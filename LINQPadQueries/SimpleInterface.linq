<Query Kind="Program" />

interface ILeft
{
    void Move();
}

    public class MoveableOject : ILeft
    {
        //without public we get an error
        public void Move()
        {
            Console.WriteLine("Left moving");
        }
    }

    static void Main()
    {
            MoveableOject mo = new MoveableOject();
            mo.Move();
            Console.Read();
    }
   