<Query Kind="Program">
  <Namespace>System</Namespace>
  <Namespace>System.Collections.Generic</Namespace>
  <Namespace>System.Linq</Namespace>
  <Namespace>System.Web</Namespace>
  <Namespace>System.Web.Script.Serialization</Namespace>
  <Namespace>System.Web.Security</Namespace>
  <Namespace>WFA.Data.IdentityTypes</Namespace>
  <Namespace>WFA.NewPortal.Membership</Namespace>
  <Namespace>WFA.NewPortal.Models</Namespace>
</Query>

void Main()
{
}


    //Just treat as a helper class.
    public class UserManager
    {

        /// <summary>
        /// Returns the User from the Context.User.Identity by decrypting the forms auth ticket and returning the user object.
        /// </summary>
        public static User User
        {
            get
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    // The user is authenticated. Return the user from the forms auth ticket.
                    return ((MyPrincipal)(HttpContext.Current.User)).User;
                }
                else if (HttpContext.Current.Items.Contains("User"))
                {
                    // The user is not authenticated, but has successfully logged in. used during initial stages of registering. 
                    return (User)HttpContext.Current.Items["User"];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Authenticates a user against a database, web service, etc.
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <returns>User</returns>
        public static User AuthenticateUser(string username, string password)
        {
            //User user = null;

            // Lookup user in database, web service, etc. We'll just generate a fake user for this demo.
            //if (username == "john" && password == "doe")
            //{
            //    user = new User { Id = 123, Name = "John Doe", Username = "johndoe"};
            //}

            return new User();
        }


		//Need to add another method similar to thee Validate User mentioned below. 
		//note string here can be anything.
		//can also be an object, rather than a string. 		
        private static readonly Dictionary<Guid, string> LoggedInUserNameCache = new Dictionary<Guid, string>();
        
        public static string FindAuthenticatedUserName(FormsAuthenticationTicket ticket){
            Guid sessionId;
            if (!Guid.TryParse(ticket.UserData, out sessionId))
            {
                throw new InvalidOperationException("The ticket could not be linked to an existing session.");
            }
            return LoggedInUserNameCache[sessionId];
        }
        
        /// <summary>
        /// Authenticates a user via the MembershipProvider and creates the associated forms authentication ticket.
        /// </summary>
        /// <param name="logon">Logon</param>
        /// <param name="response">HttpResponseBase</param>
        /// <returns>bool</returns>
        public static bool ValidateUser(LogOnModel logon, HttpResponseBase response )
        {
            bool result = false;
            var membership = System.Web.Security.Membership.Provider;
                     

            if (membership.ValidateUser(logon.UserName, logon.Password))
            {
                // create a new session id to identify the user from the ticket
                Guid sessionId = Guid.NewGuid();
                // store the username in the cache along with session id so that you can recover user data from the ticket
                LoggedInUserNameCache.Add(sessionId, logon.UserName);


                // Create the authentication ticket with custom user data.
                //var serializer = new JavaScriptSerializer();
                //string userData = serializer.Serialize(UserManager.User);
                
                
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                        logon.UserName,
                        DateTime.Now,
                        DateTime.Now.AddDays(30),
                        true,
                        sessionId.ToString(),
                        FormsAuthentication.FormsCookiePath);
                
                // Encrypt the ticket.
                string encTicket = FormsAuthentication.Encrypt(ticket);

                // Create the cookie.
                response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

                result = true;
            }

            return result;
        }

        /// <summary>
        /// Clears the user session, clears the forms auth ticket, expires the forms auth cookie.
        /// </summary>
        /// <param name="session">HttpSessionStateBase</param>
        /// <param name="response">HttpResponseBase</param>
        public static void Logoff(HttpSessionStateBase session, HttpResponseBase response)
        {
            // Delete the user details from cache.
            session.Abandon();

            // Delete the authentication ticket and sign out.
            FormsAuthentication.SignOut();

            // Clear authentication cookie.
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            response.Cookies.Add(cookie);
        }


    }
