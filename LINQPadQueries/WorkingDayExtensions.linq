<Query Kind="Program">
  <Namespace>System</Namespace>
  <Namespace>System.Collections.Generic</Namespace>
  <Namespace>System.Globalization</Namespace>
  <Namespace>System.IO</Namespace>
  <Namespace>System.Text</Namespace>
  <Namespace>System.Xml</Namespace>
  <Namespace>System.Xml.Serialization</Namespace>
</Query>

void Main()
{
	#region XML
  var xmlDoc = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiID8+DQo8SG9saWRheXM+DQogIDxIb2xpZGF5IGRheT0iMzEiIG1vbnRoPSIxMiIgeWVhcj0iMjAxNCIgZGVzY3JpcHRpb24gPSJDaHJpc3RtYXMgRGF5IDIwMTQiIC8+DQogIDxIb2xpZGF5IGRheT0iMSIgbW9udGg9IjEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTmV3IFllYXJzIERheSAyMDE1IiAvPg0KICA8SG9saWRheSBkYXk9IjE5ICIgbW9udGg9IjEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTWFydGluIEx1dGhlciBLaW5nIEpyLiBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMTYiIG1vbnRoPSIyIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9Ildhc2hpbmd0b24ncyBCaXJ0aGRheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIzIiBtb250aD0iNCIgeWVhcj0iMjAxNSIgZGVzY3JpcHRpb24gPSJHb29kIEZyaWRheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNSIgbW9udGg9IjUiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTWVtb3JpYWwgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjQiIG1vbnRoPSI3IiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IkluZGVwZW5kZW5jZSBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iNyIgbW9udGg9IjkiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTGFib3IgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjEyIiBtb250aD0iMTAiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iQ29sdW1idXMgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjExIiBtb250aD0iMTEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iVmV0ZXJhbidzIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNiIgbW9udGg9IjExIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IlRoYW5rc2dpdmluZyBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMjQiIG1vbnRoPSIxMiIgeWVhcj0iMjAxNSIgZGVzY3JpcHRpb24gPSJDaHJpc3RtYXMgRXZlIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNSIgbW9udGg9IjEyIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IkNocmlzdG1hcyBEYXkgMjAxNSIgLz4gIA0KICA8SG9saWRheSBkYXk9IjEiIG1vbnRoPSIxIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9Ik5ldyBZZWFyJ3MgRGF5IDIwMTYiIC8+DQogIDxIb2xpZGF5IGRheT0iMTgiIG1vbnRoPSIxIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9Ik1hcnRpbiBMdXRoZXIgS2luZywgSnIuIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIzMCIgbW9udGg9IjUiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iTWVtb3JpYWwgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjQiIG1vbnRoPSI3IiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9IkluZGVwZW5kZW5jZSBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iNSIgbW9udGg9IjkiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iTGFib3IgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjI0IiBtb250aD0iMTEiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iVGhhbmtzZ2l2aW5nIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNiIgbW9udGg9IjEyIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9IkNocmlzdG1hcyBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMSIgbW9udGg9IjEiIHllYXI9IjIwMTciIGRlc2NyaXB0aW9uID0iTmV3IFllYXIncyBEYXkgMjAxNyIgLz4NCjwvSG9saWRheXM+DQoNCg0K";
  #endregion
  
  HolidayCalculator.GetHolidays().Dump();
  
  
}

 /// <summary>
    /// http://mikeperetz.blogspot.com/ 
    /// </summary>
    public static class XmlSerializerHelper
    {
        public static object GetObjectFromXml(string xml, Type type)
        {
            XmlSerializer ser = new XmlSerializer(type);
            object value = ser.Deserialize(new StringReader(xml));
            return value;
        }

        public static string GetXmlFromObj(object obj, Type type)
        {
            string xml;
            XmlSerializer ser = new XmlSerializer(type);
            using (StringWriter writer = new StringWriter())
            {
                ser.Serialize(writer, obj);
                writer.Flush();
                xml = writer.ToString();
            }

            return xml;
        }

        public static string ToXml(this object obj)
        {
            Type type = obj.GetType();
            string xml = GetXmlFromObj(obj, type);
            return xml;
        }

    }

 	public static class WorkingDaysExtensions
    {
        public static bool IsWorkingDay(this DateTime dt)
        {
            switch (dt.DayOfWeek)
            {
                case DayOfWeek.Friday:
                case DayOfWeek.Monday:
                case DayOfWeek.Thursday:
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsWeekend(this DateTime dt)
        {
            return ((dt.DayOfWeek == DayOfWeek.Saturday) || (dt.DayOfWeek == DayOfWeek.Sunday)) ? true : false;
        }
        
        public static bool IsHoliday(this DateTime dt)
        {
            if (HolidayCalculator.GetHolidaysInDates().Contains(dt))
                return true;
            else
                return false;
        }

        public static DateTime PreviousWorkingDay(this DateTime dt)
        {
            do
            {
                dt = dt.AddDays(-1);
            } while ((dt.IsHoliday() || dt.IsWeekend()) && !dt.IsWorkingDay());
            //keep decrementing until I find a working day which is not a weekend nor a holiday.

            return dt;
        }

        public static DateTime NextWorkingDay(this DateTime dt)
        {
            do
            {
                dt = dt.AddDays(1);
            } while ((dt.IsHoliday() || dt.IsWeekend()) && !dt.IsWorkingDay());
            //keep decrementing until I find a working day which is not a weekend nor a holiday.
            return dt;
        }

    }
	
	public static class HolidayCalculator
    {
        //private static string MappingPath = Path.Combine(ConfigurationManager.AppSettings["MappingsPath"], "Holidays.xml");

        public static List<Holiday> GetHolidays()
        {
			var xml = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiID8+DQo8SG9saWRheXM+DQogIDxIb2xpZGF5IGRheT0iMzEiIG1vbnRoPSIxMiIgeWVhcj0iMjAxNCIgZGVzY3JpcHRpb24gPSJDaHJpc3RtYXMgRGF5IDIwMTQiIC8+DQogIDxIb2xpZGF5IGRheT0iMSIgbW9udGg9IjEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTmV3IFllYXJzIERheSAyMDE1IiAvPg0KICA8SG9saWRheSBkYXk9IjE5ICIgbW9udGg9IjEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTWFydGluIEx1dGhlciBLaW5nIEpyLiBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMTYiIG1vbnRoPSIyIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9Ildhc2hpbmd0b24ncyBCaXJ0aGRheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIzIiBtb250aD0iNCIgeWVhcj0iMjAxNSIgZGVzY3JpcHRpb24gPSJHb29kIEZyaWRheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNSIgbW9udGg9IjUiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTWVtb3JpYWwgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjQiIG1vbnRoPSI3IiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IkluZGVwZW5kZW5jZSBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iNyIgbW9udGg9IjkiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTGFib3IgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjEyIiBtb250aD0iMTAiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iQ29sdW1idXMgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjExIiBtb250aD0iMTEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iVmV0ZXJhbidzIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNiIgbW9udGg9IjExIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IlRoYW5rc2dpdmluZyBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMjQiIG1vbnRoPSIxMiIgeWVhcj0iMjAxNSIgZGVzY3JpcHRpb24gPSJDaHJpc3RtYXMgRXZlIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNSIgbW9udGg9IjEyIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IkNocmlzdG1hcyBEYXkgMjAxNSIgLz4gIA0KICA8SG9saWRheSBkYXk9IjEiIG1vbnRoPSIxIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9Ik5ldyBZZWFyJ3MgRGF5IDIwMTYiIC8+DQogIDxIb2xpZGF5IGRheT0iMTgiIG1vbnRoPSIxIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9Ik1hcnRpbiBMdXRoZXIgS2luZywgSnIuIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIzMCIgbW9udGg9IjUiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iTWVtb3JpYWwgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjQiIG1vbnRoPSI3IiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9IkluZGVwZW5kZW5jZSBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iNSIgbW9udGg9IjkiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iTGFib3IgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjI0IiBtb250aD0iMTEiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iVGhhbmtzZ2l2aW5nIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNiIgbW9udGg9IjEyIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9IkNocmlzdG1hcyBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMSIgbW9udGg9IjEiIHllYXI9IjIwMTciIGRlc2NyaXB0aW9uID0iTmV3IFllYXIncyBEYXkgMjAxNyIgLz4NCjwvSG9saWRheXM+DQoNCg0K";
			var data = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(xml));  
  			var xmlDoc = XDocument.Parse(data);
            //XDocument xDoc = XDocument.Load(xmlDoc);
            var holidayList = new List<Holiday>();

            var holidays = from item in xmlDoc.Descendants("Holiday")
                           select new
                           {
                               day = item.Attributes("day").FirstOrDefault().Value,
                               month = item.Attributes("month").FirstOrDefault().Value,
                               year = item.Attributes("year").FirstOrDefault().Value,
                               Date = new DateTime(int.Parse(item.Attributes("year").FirstOrDefault().Value), int.Parse(item.Attributes("month").FirstOrDefault().Value), int.Parse(item.Attributes("day").FirstOrDefault().Value)),
                               desc = item.Attributes("description").FirstOrDefault().Value
                           };

            foreach (var item in holidays)
            {
                Console.WriteLine(item.day + " - " + item.month + " - " + item.year + " - " + item.desc + " - ");
                holidayList.Add(new Holiday
                {
                    Day = int.Parse(item.day),
                    Month = int.Parse(item.month),
                    Year = int.Parse(item.year),
                    Description = item.desc
                }
                    );
            }
            return holidayList;
        }

        public static List<Holiday> GetHolidayCollection()
        {
		var xmlDoc = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiID8+DQo8SG9saWRheXM+DQogIDxIb2xpZGF5IGRheT0iMzEiIG1vbnRoPSIxMiIgeWVhcj0iMjAxNCIgZGVzY3JpcHRpb24gPSJDaHJpc3RtYXMgRGF5IDIwMTQiIC8+DQogIDxIb2xpZGF5IGRheT0iMSIgbW9udGg9IjEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTmV3IFllYXJzIERheSAyMDE1IiAvPg0KICA8SG9saWRheSBkYXk9IjE5ICIgbW9udGg9IjEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTWFydGluIEx1dGhlciBLaW5nIEpyLiBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMTYiIG1vbnRoPSIyIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9Ildhc2hpbmd0b24ncyBCaXJ0aGRheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIzIiBtb250aD0iNCIgeWVhcj0iMjAxNSIgZGVzY3JpcHRpb24gPSJHb29kIEZyaWRheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNSIgbW9udGg9IjUiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTWVtb3JpYWwgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjQiIG1vbnRoPSI3IiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IkluZGVwZW5kZW5jZSBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iNyIgbW9udGg9IjkiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTGFib3IgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjEyIiBtb250aD0iMTAiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iQ29sdW1idXMgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjExIiBtb250aD0iMTEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iVmV0ZXJhbidzIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNiIgbW9udGg9IjExIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IlRoYW5rc2dpdmluZyBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMjQiIG1vbnRoPSIxMiIgeWVhcj0iMjAxNSIgZGVzY3JpcHRpb24gPSJDaHJpc3RtYXMgRXZlIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNSIgbW9udGg9IjEyIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IkNocmlzdG1hcyBEYXkgMjAxNSIgLz4gIA0KICA8SG9saWRheSBkYXk9IjEiIG1vbnRoPSIxIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9Ik5ldyBZZWFyJ3MgRGF5IDIwMTYiIC8+DQogIDxIb2xpZGF5IGRheT0iMTgiIG1vbnRoPSIxIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9Ik1hcnRpbiBMdXRoZXIgS2luZywgSnIuIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIzMCIgbW9udGg9IjUiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iTWVtb3JpYWwgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjQiIG1vbnRoPSI3IiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9IkluZGVwZW5kZW5jZSBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iNSIgbW9udGg9IjkiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iTGFib3IgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjI0IiBtb250aD0iMTEiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iVGhhbmtzZ2l2aW5nIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNiIgbW9udGg9IjEyIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9IkNocmlzdG1hcyBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMSIgbW9udGg9IjEiIHllYXI9IjIwMTciIGRlc2NyaXB0aW9uID0iTmV3IFllYXIncyBEYXkgMjAxNyIgLz4NCjwvSG9saWRheXM+DQoNCg0K";
            XDocument xDoc = XDocument.Load(xmlDoc);

            var holidays = from item in xDoc.Descendants("Holiday")
                           select new Holiday()
                           {
                               Day = int.Parse(item.Attributes("day").FirstOrDefault().Value),
                               Month = int.Parse(item.Attributes("month").FirstOrDefault().Value),
                               Year = int.Parse(item.Attributes("year").FirstOrDefault().Value),
                               Date = new DateTime(int.Parse(item.Attributes("year").FirstOrDefault().Value), int.Parse(item.Attributes("month").FirstOrDefault().Value), int.Parse(item.Attributes("day").FirstOrDefault().Value)),
                               Description = item.Attributes("description").FirstOrDefault().Value
                           };

            List<Holiday> holidayList = holidays.ToList();
            return holidayList;
        }

        public static List<DateTime> GetHolidaysInDates()
        {
			var xmlDoc = "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiID8+DQo8SG9saWRheXM+DQogIDxIb2xpZGF5IGRheT0iMzEiIG1vbnRoPSIxMiIgeWVhcj0iMjAxNCIgZGVzY3JpcHRpb24gPSJDaHJpc3RtYXMgRGF5IDIwMTQiIC8+DQogIDxIb2xpZGF5IGRheT0iMSIgbW9udGg9IjEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTmV3IFllYXJzIERheSAyMDE1IiAvPg0KICA8SG9saWRheSBkYXk9IjE5ICIgbW9udGg9IjEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTWFydGluIEx1dGhlciBLaW5nIEpyLiBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMTYiIG1vbnRoPSIyIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9Ildhc2hpbmd0b24ncyBCaXJ0aGRheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIzIiBtb250aD0iNCIgeWVhcj0iMjAxNSIgZGVzY3JpcHRpb24gPSJHb29kIEZyaWRheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNSIgbW9udGg9IjUiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTWVtb3JpYWwgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjQiIG1vbnRoPSI3IiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IkluZGVwZW5kZW5jZSBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iNyIgbW9udGg9IjkiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iTGFib3IgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjEyIiBtb250aD0iMTAiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iQ29sdW1idXMgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjExIiBtb250aD0iMTEiIHllYXI9IjIwMTUiIGRlc2NyaXB0aW9uID0iVmV0ZXJhbidzIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNiIgbW9udGg9IjExIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IlRoYW5rc2dpdmluZyBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMjQiIG1vbnRoPSIxMiIgeWVhcj0iMjAxNSIgZGVzY3JpcHRpb24gPSJDaHJpc3RtYXMgRXZlIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNSIgbW9udGg9IjEyIiB5ZWFyPSIyMDE1IiBkZXNjcmlwdGlvbiA9IkNocmlzdG1hcyBEYXkgMjAxNSIgLz4gIA0KICA8SG9saWRheSBkYXk9IjEiIG1vbnRoPSIxIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9Ik5ldyBZZWFyJ3MgRGF5IDIwMTYiIC8+DQogIDxIb2xpZGF5IGRheT0iMTgiIG1vbnRoPSIxIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9Ik1hcnRpbiBMdXRoZXIgS2luZywgSnIuIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIzMCIgbW9udGg9IjUiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iTWVtb3JpYWwgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjQiIG1vbnRoPSI3IiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9IkluZGVwZW5kZW5jZSBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iNSIgbW9udGg9IjkiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iTGFib3IgRGF5IiAvPg0KICA8SG9saWRheSBkYXk9IjI0IiBtb250aD0iMTEiIHllYXI9IjIwMTYiIGRlc2NyaXB0aW9uID0iVGhhbmtzZ2l2aW5nIERheSIgLz4NCiAgPEhvbGlkYXkgZGF5PSIyNiIgbW9udGg9IjEyIiB5ZWFyPSIyMDE2IiBkZXNjcmlwdGlvbiA9IkNocmlzdG1hcyBEYXkiIC8+DQogIDxIb2xpZGF5IGRheT0iMSIgbW9udGg9IjEiIHllYXI9IjIwMTciIGRlc2NyaXB0aW9uID0iTmV3IFllYXIncyBEYXkgMjAxNyIgLz4NCjwvSG9saWRheXM+DQoNCg0K";
            XDocument xDoc = XDocument.Load(xmlDoc);


            var holidays = from item in xDoc.Descendants("Holiday")
                           select new Holiday()
                           {
                               Day = int.Parse(item.Attributes("day").FirstOrDefault().Value),
                               Month = int.Parse(item.Attributes("month").FirstOrDefault().Value),
                               Year = int.Parse(item.Attributes("year").FirstOrDefault().Value),
                               Date = new DateTime(int.Parse(item.Attributes("year").FirstOrDefault().Value), int.Parse(item.Attributes("month").FirstOrDefault().Value), int.Parse(item.Attributes("day").FirstOrDefault().Value)),
                               Description = item.Attributes("description").FirstOrDefault().Value
                           };

            List<Holiday> holidayList = holidays.ToList();

            var query = from item in holidayList
                        select new { item.Date };


            List<DateTime> dateList = new List<DateTime>();
            foreach (var item in query)
            {
                dateList.Add(item.Date);
            }

            return dateList;
        }

        public static string ValidatedDate(string stmpos_date)
        {
            int day= 0, month = 0, year = 0;
            DateTime dt = DateTime.MinValue;
            if (stmpos_date.IsNumeric())
            {
                day = int.Parse(stmpos_date.Substring(stmpos_date.Length - 2, 2));
                month = int.Parse(stmpos_date.Substring(4, 2));
                year = int.Parse(stmpos_date.Substring(0, 4));
                dt = new DateTime(year, month, day);

                if (dt.IsWeekend() || dt.IsHoliday())
                    dt = dt.PreviousWorkingDay();
                stmpos_date = dt.ToString("yyyyMMdd");              
            }         
            return stmpos_date;
        }

    }
	
	public static class StringExtensions
    {
        public static bool IsNumeric(this string theValue)
        {
            long retNum;
            return long.TryParse(theValue, System.Globalization.NumberStyles.Integer, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
        }

        public static DateTime? ToDateTime(this string s)
        {
            DateTime dtr;
            var tryDtr = DateTime.TryParse(s, out dtr);
            return (tryDtr) ? dtr : new DateTime?();
        }

        //http://www.codeproject.com/Articles/692603/Csharp-String-Extensions
        public static double ToDouble(this string input, bool throwExceptionIfFailed = false)
        {
            double result;
            var valid = double.TryParse(input, NumberStyles.AllowDecimalPoint,
              new NumberFormatInfo { NumberDecimalSeparator = "." }, out result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException(string.Format("'{0}' cannot be converted as double", input));
            return result;

        }

        public static decimal GetDecimal(this string val)
        {
            decimal d;
            if (!decimal.TryParse(val, out d))
            {
                d = 0.0M;
            }
            return d;
        }

        public static int ToInt(this string @string)
        {
            return int.Parse(@string);
        }

        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public static bool IsNullOrBlank(this String text)
        {
            return text == null || text.Trim().Length == 0;
        }

    }
	
	public class Holiday
    {
        public int Month { get; set; }
        public int Day { get; set; }
        public int Year { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
    }