<Query Kind="Program" />

void Main()
{
	//ISD = SQL Server Name  = whatever company name your server is.
	//invstorportal_data = Database Name - is your database name you connect to. 
	//BREntries is the table you want to extract to CSV.
	//C:\\BCP\\TestDumper.csv - the filepath and filename you want to extract to.
	
	var connectionString = "Data Source=ISD;Initial Catalog=invstorportal_data;Integrated Security=True";
	using (SqlConnection conn = new SqlConnection(connectionString))
	{
			conn.Open();
			TableDumper.DumpTableToFile(conn
								,"BREntries"
								, "C:\\BCP\\TestDumper.csv");
								
	}
								
}


public static class TableDumper
{
    public static void DumpTableToFile(SqlConnection connection, string tableName, string destinationFile)
    {
        using (var command = new SqlCommand("select * from " + tableName, connection))
        using (var reader = command.ExecuteReader())
        using (var outFile = File.CreateText(destinationFile))
        {
            string[] columnNames = GetColumnNames(reader).ToArray();
            int numFields = columnNames.Length;
            outFile.WriteLine(string.Join(",", columnNames));
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string[] columnValues = 
                        Enumerable.Range(0, numFields)
                                  .Select(i => reader.GetValue(i).ToString())
                                  .Select(field => string.Concat("\"", field.Replace("\"", "\"\""), "\""))
                                  .ToArray();
                    outFile.WriteLine(string.Join(",", columnValues));
                }
            }
        }
    }
    private static IEnumerable<string> GetColumnNames(IDataReader reader)
    {
        foreach (DataRow row in reader.GetSchemaTable().Rows)
        {
            yield return (string)row["ColumnName"];
        }
    }
}

