<Query Kind="Program" />

void Main()
{
	//string myString = " blah blah\r\r\r blah blah       \n\n blah   ";
	
	string myString = "AIR/SPRING ACTUATOR.*456";
	Console.WriteLine(myString);
		
	myString = Regex.Replace(myString, @"[\u000A\u000B\u000C\u000D\u2028\u2029\u0085]+", String.Empty);
	Console.WriteLine(myString);
	
	myString = RemoveWhiteSpaceLineEndingsCarriageReturnsLineFeedsDots(myString);
	
	myString = Regex.Replace(myString, @"\s+", " ");
	Console.WriteLine(myString);
}


public string RemoveWhiteSpaceLineEndingsCarriageReturnsLineFeedsDots(string value)
{
	if (String.IsNullOrEmpty(value))
	{
		return value;
	}
	string lineSeparator = ((char)0x2028).ToString();
	string paragraphSeparator = ((char)0x2029).ToString();

	return value.Replace("\r\n", string.Empty)
				.Replace("\n", string.Empty)
				.Replace("\r", string.Empty)
				.Replace(lineSeparator, string.Empty)
				.Replace(paragraphSeparator, string.Empty)
				.Replace("\"", "")
				.Replace(".", string.Empty)
				.Replace("*", string.Empty)
				.TrimStart()
				.TrimEnd()
				.Trim();
}
// Define other methods and classes here